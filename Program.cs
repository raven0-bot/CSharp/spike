﻿using System;
using System.IO;

namespace filecreator
{
    class Program
    {
        public static void Main(string[] args)
        {
            retry:
            Console.Clear();
            Console.WriteLine("Choose an option:");
            Console.WriteLine("\n1) Append / Write File");
            Console.WriteLine("\n2) Read File");

            switch (Console.ReadLine()) {
                case "1":
                    AppendFile();
                    break;
                case "2":
                    ReadFile();
                    break;
                default:
                    Console.WriteLine("That is not an option");
                    goto retry;
            }
        }
        private static void AppendFile() {
            Console.Clear();
            retry:
            Console.WriteLine("Please enter a name for the file...");

            string Name = Console.ReadLine();
            string name = Name.Trim();

            if (name == "") {
                Console.WriteLine("No name provided please try again!");
                goto retry;
            }
   
            Console.WriteLine("Type anything you want to append to this file...");
            Console.WriteLine("To exit please type .exit()");
            string text = Console.ReadLine();
            while (text != ".exit()") {
                File.AppendAllText(name, text + Environment.NewLine);
                text = Console.ReadLine();
            }
        }
        private static void ReadFile() {
            Console.Clear();
            retry:
            Console.WriteLine("Type the name of the file you want to read...");
            string FileName = Console.ReadLine();
            string filename = FileName.Trim();

            if (filename == "") {
                Console.WriteLine("No filename given. Try again!");
                goto retry;
            }
            
            var fileRead = File.ReadAllLines(filename);

            Console.Clear();
            Console.WriteLine("Contents of: " + filename);
            foreach (string line in fileRead) {
                Console.WriteLine(line);
            }


            Console.WriteLine("\nPress any key to quit");
            Console.ReadKey(true);
        }   
    }
}
